# Binar Car Rental : Challange 3 

## Brief Explanation
Static website with a Rent Car theme created from the slicing of [Figma Binar](https://www.figma.com/file/QiNXZPX7OwUeFzqSPuiQBE/BCR---Binar-Car-Rental?node-id=18293%3A5613). Consists of the src folder, which stores the picture, index.html, and index.css file. This program has been designed responsively for desktop, tablet, and mobile users.

## Implementation
* Navigation bar
    * When Our Services is clicked, the display will be directed directly to  Best Car Rental for any kind of trip in
    (Lokasimu)!
    * When Why Us is clicked, the display will be directed directly to the Why Us?
    * When Testimonials are clicked, the display will immediately be directed to the Testimonials section
    * When the FAQ is clicked, the view will be directed to the Frequently Asked Questions section
* Carousel in the Testimonials section. Each card will move according to the direction when the next and previous buttons.
*  Create accordions in the Frequently Asked Questions section of each accordion. When clicked, a dummy text will appear, it could be lorem ipsum.

## Test Responsive Design 
1. Click right "Inspect"
2. Click mobile icon "Responsive Design Mode"
3. Choose mobile or ipad mode
4. Or you can just shrink the window size

## Tech 
1. HTML 5
2. CSS
3. Bootstrap 5